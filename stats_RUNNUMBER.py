#! /usr/bin/env python

import stats
import numpy
import glob
import string
import sys
import os

folder='./'
if len(sys.argv) > 1:
	folder=sys.argv[1]
if folder[-1]!='/':
	folder+='/'

fin=open("prefix.dat", "r");
prefixes = fin.read().split();
outfiles = prefixes[:];
for j in range(0, len(prefixes)):
	outfiles[j] = open(prefixes[j]+"_stats.dat", "w");

stats_section=numpy.loadtxt("stats_section.dat");
assert(numpy.shape(stats_section)[1]==2);
num_versions=numpy.shape(stats_section)[0];
print num_versions

for vind in range(num_versions):
	v=int(stats_section[vind,0]);
	print "v"+str(v);
	files=glob.glob(folder+"v"+str(v)+"/build/E.out");
	cutval=stats_section[vind,1];
	if cutval < 0:
		continue
	for f in files:
		for j in range(0, len(prefixes)):
			try:
				if prefixes[j][:4]=='FRAC':
					pts=prefixes[j].split('_');
					fn=string.replace(f,'E',pts[1]);
					fd=string.replace(f,'E',pts[2]);
					print fn + ' divided by ' + fd
					if os.path.getsize(fn) == 0 or os.path.getsize(fd) == 0:
						print 'Unable to get stats - there is no data in the file'
						st=None
					else:
						ns=int(pts[3])
						data=numpy.loadtxt(fn)
						mn=stats.SectionMeans(data[cutval:],ns)
						data=numpy.loadtxt(fd)
						md=stats.SectionMeans(data[cutval:],ns)
						frac=numpy.zeros((len(mn)))
						for k in range(len(mn)):
							frac[k]=mn[k]/md[k];
							st=stats.Stats(frac);
				else:
					fnew=string.replace(f, "E", prefixes[j]);
					print fnew
					if os.path.getsize(fnew) == 0:
						print 'Unable to get stats - there is no data in the file'
						st=None
					else:
						data=numpy.loadtxt(fnew);
						assert len(data) > abs(cutval)
						st=stats.Stats(data[cutval:]);
					
				if st != None:
					outfiles[j].write(str(v)+'\t');
					for s in st:
						outfiles[j].write(str(s)+'\t');
					outfiles[j].write('\n');

			except IOError:
				print "IOError encountered - moving on to next file"
						
			except OSError:
				print "OSError encountered - moving on to next file"
						
