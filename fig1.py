from mkplot_all import *
import sys
import subprocess

folder='/net/gp/ph/1/mxk177330/data/RUNNUMBER/'

mkplot.plotseries(pylab.gca(),'x','y',[])
font=matplotlib.font_manager.FontProperties(size='small')
leg=pylab.legend(prop=font,loc='best')
mkplot.set_tick_size(pylab.gca(),'large')
pylab.xlabel(r'',fontsize='medium')
pylab.ylabel(r'',fontsize='medium')
fig_file='RUNNUMBER_fig1.png'
pylab.savefig(fig_file)
# pylab.savefig(fig_file.replace('png','pdf'))
if sys.argv[-1]=='show':
	pylab.show()

sys.exit()

#Other potentially useful code
vmap=numpy.loadtxt('versionmap.dat',skiprows=1)
labels=open('versionmap.dat').readline().split()
A_col=labels.index('A')

for vnum in xrange(vmap.shape[0]):
	A=int(vmap[vnum,A_col])
