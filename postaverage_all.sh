if test $# -lt 2
    then
    echo "Proper usage: postaverage_all.sh NUM_VERSIONS NUM_AVG [Backup=Y/N]"
    exit 0
fi

if [ "$#" == "3" ]
    then
    if [ "$3" == "Y" ]
	then
	for((i=1;i<$1;i=i+1))
	  do
	  cmd=`printf "cp -r v%s/ v%s_backup/" "$i" "$i"`
	  echo $cmd
	  $cmd
	done	
    fi
fi

for((i=1;i<$1;i=i+1))
  do
  template=`printf "v%s/build/*.out" "$i"`
  for file in $template
    do
    cmd=`printf "postaverage.py %s %s" "$file" "$2"`
    echo $cmd
    $cmd
  done
done